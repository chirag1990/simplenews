//
//  ArticleViewModel.swift
//  Simple News
//
//  Created by Chirag Tailor on 04/06/2021.
//

import Foundation

struct ArticleListViewModel {
    let articles: [Article]
}

extension ArticleListViewModel {
    var numberOfSection: Int {
        return 1
    }
    
    func numberOfRowInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        let article = self.articles[index]
        return ArticleViewModel(article)
    }
}

struct ArticleViewModel {
    private let article: Article
}

extension ArticleViewModel {
    init(_ article: Article) {
        self.article = article
    }
}

extension ArticleViewModel {
    var title: String {
        return self.article.title ?? ""
    }
    
    var description: String {
        return self.article.description ?? ""
    }
    
    var imageUrl: String {
        return self.article.urlToImage ?? ""
    }
    
    var publishedAt: String {
        return formattedDateFromString(dateString: self.article.publishedAt!) ?? ""
    }
    
    func formattedDateFromString(dateString: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "dd-MM-yyyy HH:mm"
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
}
