//
//  NewsService.swift
//  Simple News
//
//  Created by Chirag Tailor on 04/06/2021.
//

import Foundation

class NewsService {
    
    func getArticles(url: URL, completion: @escaping ([Article]?) -> ()) {
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                print(error)
                completion(nil)
            } else if let data = data {
                do {
                    let articlesList = try JSONDecoder().decode(ArticlesList.self, from: data)
                    completion(articlesList.articles)
                    
                } catch {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }
        }.resume()
    }
}
