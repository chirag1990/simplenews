//
//  Article.swift
//  Simple News
//
//  Created by Chirag Tailor on 04/06/2021.
//

import Foundation

struct ArticlesList: Decodable {
    let articles: [Article]
}

struct Article: Decodable {
    let title: String?
    let description: String?
    let urlToImage: String?
    let publishedAt: String?
}
