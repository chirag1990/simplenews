//
//  ArticleTableViewCell.swift
//  Simple News
//
//  Created by Chirag Tailor on 04/06/2021.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var publishedAtLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        descriptionLabel.text = ""
        newsImageView.image = nil
    }
    
    func configureCell(with articleVM: ArticleViewModel) {
        titleLabel.text = articleVM.title
        descriptionLabel.text = articleVM.description
        publishedAtLabel.text = articleVM.publishedAt
        
        if let url = URL(string: articleVM.imageUrl) {
            UIImage.loadFrom(url: url) { image in
                self.newsImageView.image = image
            }
        } else {
            self.newsImageView.image = UIImage(named: "news")
        }
    }
}
